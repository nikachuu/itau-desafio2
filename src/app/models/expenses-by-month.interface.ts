export interface IExpensesByMonth {
    month: number;
    totalValue: number;
}