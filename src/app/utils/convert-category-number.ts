import { ECategories } from '../enums/categories.enum';

export function convertCategoryNumber(number: number): string {
    return ECategories[number];
}
