import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IGeneralExpensesState } from '../state/general-expenses.state'

const selectGeneralExpensesState = (state: IAppState) => state.generalExpenses;

export const selectGeneralExpenses = createSelector(
    selectGeneralExpensesState,
    (state: IGeneralExpensesState) => state.generalExpenses
)