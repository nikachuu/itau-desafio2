import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { FinancialPostingsService } from '../../services/financial-postings.service';
import {
    EGeneralExpensesActions,
    GetGeneralExpenses,
    GetGeneralExpensesSuccess,
    SetGeneralExpensesError
} from '../actions/general-expenses.actions';
import { HttpResponse } from '@angular/common/http';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';

@Injectable()
export class GeneralExpensesEffects{
    constructor(
        private _service: FinancialPostingsService,
        private _actions: Actions
    ) { }

    @Effect()
    getGeneralExpenses$ = this._actions.pipe(
        ofType<GetGeneralExpenses>(EGeneralExpensesActions.GetGeneralExpenses),
        mergeMap(() =>
            this._service.GetFinancialPostings().pipe(
                map((res: HttpResponse<IFinancialPostings[]>) => {
                   if (res.status === 200) {
                       return new GetGeneralExpensesSuccess(res.body)
                   } 
                }),
                catchError(error => [console.error(error)])
            )
        )
    );
}