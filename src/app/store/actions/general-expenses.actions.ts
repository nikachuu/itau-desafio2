import { Action } from '@ngrx/store';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';

export enum EGeneralExpensesActions {
    GetGeneralExpenses = '[GET] General Expenses',
    GetGeneralExpensesSuccess = '[GET] General Expenses Success',
    SetGeneralExpensesError = '[ERROR] General Expenses Error'
}

export class GetGeneralExpenses implements Action {
    public readonly type = EGeneralExpensesActions.GetGeneralExpenses;
}

export class GetGeneralExpensesSuccess implements Action {
    public readonly type = EGeneralExpensesActions.GetGeneralExpensesSuccess;
    constructor(public payload: IFinancialPostings[]) { }
}

/*
* Como não foi possível reproduzir erros na requisição, essa Action é apenas demonstrativa
*/
export class SetGeneralExpensesError implements Action {
    public readonly type = EGeneralExpensesActions.SetGeneralExpensesError;
    constructor (public payload: any) { }
}

export type GeneralExpensesActions =
    GetGeneralExpenses |
    GetGeneralExpensesSuccess |
    SetGeneralExpensesError;