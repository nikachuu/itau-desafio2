import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { generalExpensesReducer } from './general-expenses.reducer';

export const appReducers: ActionReducerMap<IAppState, any> = {
    generalExpenses: generalExpensesReducer
}