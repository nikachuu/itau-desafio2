import { GeneralExpensesActions, EGeneralExpensesActions} from '../actions/general-expenses.actions';
import { IGeneralExpensesState, initialGeneralExpensesState } from '../state/general-expenses.state';

export function generalExpensesReducer(
    state = initialGeneralExpensesState,
    action: GeneralExpensesActions
): IGeneralExpensesState {

    switch (action.type) {
        case EGeneralExpensesActions.GetGeneralExpensesSuccess: {
            return {
                ...state,
                generalExpenses: action.payload
            }
        }
        default:
            return state;
    }
}