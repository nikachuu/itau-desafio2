import { IGeneralExpensesState, initialGeneralExpensesState } from './general-expenses.state';

export interface IAppState {
    generalExpenses: IGeneralExpensesState;
}

export const initialAppState: IAppState = {
    generalExpenses: initialGeneralExpensesState
}

export function getInitialState(): IAppState {
    return initialAppState;
}