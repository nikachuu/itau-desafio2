import { IFinancialPostings } from 'src/app/models/financial-postings.interface';

export interface IGeneralExpensesState {
    generalExpenses: IFinancialPostings[]
}

export const initialGeneralExpensesState: IGeneralExpensesState = {
    generalExpenses: null
}