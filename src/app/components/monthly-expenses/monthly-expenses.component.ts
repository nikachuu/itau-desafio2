import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { selectGeneralExpenses } from 'src/app/store/selectors/general-expenses.selectors';
import { GetGeneralExpenses } from 'src/app/store/actions/general-expenses.actions';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';
import { IExpensesByMonth } from 'src/app/models/expenses-by-month.interface';
import { convertCategoryNumber } from '../../utils/convert-category-number';
import { convertMonthNumber } from '../../utils/convert-month-number';

@Component({
  selector: 'app-monthly-expenses',
  templateUrl: './monthly-expenses.component.html',
  styleUrls: ['./monthly-expenses.component.scss']
})
export class MonthlyExpensesComponent implements OnInit {

  constructor(
    private _store: Store<IAppState>
  ) { }

  selectGeneralExpenses$ = this._store.pipe(select(selectGeneralExpenses));
  monthlyExpenses: IExpensesByMonth[];

  displayedColumns: string[] = ['month', 'spent_value'];

  convertCategoryNumber = convertCategoryNumber;
  convertMonthNumber = convertMonthNumber;

  ngOnInit(): void {
    this.selectGeneralExpenses$.subscribe((generalExpenses: IFinancialPostings[]) => {
      if (generalExpenses == null) {
        this._store.dispatch(new GetGeneralExpenses())
      } else {
        this.monthlyExpenses = this.sortExpensesByMonth(generalExpenses)
                                   .filter(index => index.totalValue > 0);
      }      
    })
  }

  public sortExpensesByMonth(expenses: IFinancialPostings[]) {
    const expensesByMonth = this.setExpensesObject();

    expenses.forEach(expense => {
      expensesByMonth.forEach(months => {
        if (expense.mes_lancamento === months.month) {
          months.totalValue += expense.valor
        }
      })
    });

    return expensesByMonth;
  }

  public setExpensesObject(): IExpensesByMonth[] {
    const expensesByMonth = [];
    for (let i = 1; i <= 12; i++) {
      expensesByMonth.push({
        month: i,
        totalValue: 0
      })
    }

    return expensesByMonth;
  }
}
