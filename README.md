# Desafio 2 - Itaú

Olá! :)\
Gostaria muito de agradecer a oportunidade de participar do processo do Itaú!

## Frameworks de escolha 

- **Angular 9**
- **NgRx**, para gerenciamento de Estados/Side Effects e para ter melhor comunicação entre componentes
- **Angular Material** como Framework de CSS
- **Normalize.css** para 'resetar' comportamento padrão de folhas de estilo

## Arquitetura de projeto
- Reproduzi a arquitetura que estou acostumada a lidar no trabalho:
    - **components**: folder responsável por armazenar cada componente do projeto; normalmente eu também teria uma pasta pages, mas não vi necessidade por se tratar de um projeto pequeno
    - **enums**: armazena os arquivos responsaveis por mapear as Categorias e Meses de Lançamento, para poder converter tudo pra string posteriormente
    - **models**: armazena os arquivos de interface
    - **services**: armazena os serviços; nesse caso, a comunicação com a API se inicia por aqui
    - **store**: onde possui toda a arquitetura do NgRx, com actions, effects, reducers, selectors e state
    - **utils**: armazena funções de lógica pequena e útil (assim como o nome da pasta indica) que são utilizadas por mais de um componente

## Build e deploy

- O projeto foi buildado e deployado nesse [link do netlify](https://alinesantos-itaud2.netlify.com/)
- Como padrão, também pode ser rodado localmente após npm install e rodando `npm start` ou `ng serve --open`

## Considerações gerais

- Esse possui lógica mais básica e similar à do desafio 1, e não leva tanto em consideração os pilares de programação funcional, e isso é proposital, porque assim acredito que
a diferença fica bem evidente quando comparado ao código do desafio à seguir. :)


É isso!\
Aguardando retorno. :)\
Bora pro desafio final?\
\
*Aline de Lima Santos*